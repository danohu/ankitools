import click#
import csv

@click.command()
@click.argument('infile')
@click.argument('outfile')
def process(infile, outfile):
    print(infile, outfile)
    ready = []
    fieldnames = ['Text', 'Extra', 'Tags']
    defaults = {
        'Tags': 'goodreads'
    }
    with open(infile) as inf:
        reader = csv.DictReader(inf)
        for row in reader:
            ready.append(handle_row(row, defaults))
    with open(outfile, 'w') as outf:
        writecsv(ready, outf, fieldnames)


def handle_row(inp, defaults={}):
    row = defaults.copy()
    text = '''
    Book: %(Title)s (%(Original Publication Year)s)
    Author: {{c1::%(Author)s}}''' % (inp)
    row['Text'] = text
    return row

def writecsv(dictlist, csvfile, fieldnames=None):
    if fieldnames is None:
        fieldnames = sorted(dictlist[0].keys())
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerows(dictlist)

if __name__ == '__main__':
    process()
